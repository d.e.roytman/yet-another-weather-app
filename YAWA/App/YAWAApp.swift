//
//  YAWAApp.swift
//  YAWA
//
//  Created by Dmitry Roytman on 09.05.2022.
//

import SwiftUI

@main
struct YAWAApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
