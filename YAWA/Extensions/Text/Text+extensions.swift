//
//  Text+extensions.swift
//  YAWA
//
//  Created by Dmitry Roytman on 12.05.2022.
//

import SwiftUI

extension Text {
  init(string: String) {
    self.init(string)
  }
}
