//
//  Color+extensions.swift
//  YAWA
//
//  Created by Dmitry Roytman on 12.05.2022.
//

import SwiftUI

extension Color {
  static var background: Color {
    Color(hue: 0.656, saturation: 0.787, brightness: 0.354)
  }
}
