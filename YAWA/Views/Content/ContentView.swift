//
//  ContentView.swift
//  YAWA
//
//  Created by Dmitry Roytman on 09.05.2022.
//

import SwiftUI

struct ContentView: View {
  @StateObject var locationStore = LocationStore()
  var body: some View {
    ZStack {
      Color.background.ignoresSafeArea()
      VStack {
        WelcomeView()
          .environmentObject(locationStore)
      }
    }
    .onAppear { locationStore.start() }
    .onDisappear { locationStore.stop() }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
      .preferredColorScheme(.dark)
  }
}
