//
//  CLLocationCoordinate2D+Identifiable.swift
//  YAWA
//
//  Created by Dmitry Roytman on 12.05.2022.
//

import CoreLocation

extension CLLocationCoordinate2D: Identifiable {
  public var id: String {
    "\(longitude), \(latitude)"
  }
}
