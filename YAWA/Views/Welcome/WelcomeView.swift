//
//  WelcomeView.swift
//  YAWA
//
//  Created by Dmitry Roytman on 12.05.2022.
//

import SwiftUI
import CoreLocationUI

struct WelcomeView: View {
  @EnvironmentObject var locationStore: LocationStore
  var body: some View {
    VStack {
      WelcomeHeaderView()
      WelcomeProgressView(isPresented: $locationStore.isLoading)
      CurrentLocationButton()
        .environmentObject(locationStore)
    }
    .stretch()
    .sheet(item: $locationStore.location, content: LocationInfoView.init)
  }
}

struct WelcomeView_Previews: PreviewProvider {
  static var previews: some View {
    WelcomeView()
      .environmentObject(LocationStore())
    ContentView()
      .preferredColorScheme(.dark)
  }
}

// MARK: - Helpers

extension View {
  fileprivate func stretch() -> some View {
    frame(
      maxWidth: .infinity,
      maxHeight: .infinity
    )
  }
}
