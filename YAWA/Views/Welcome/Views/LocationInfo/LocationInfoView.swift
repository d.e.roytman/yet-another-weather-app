//
//  LocationInfoView.swift
//  YAWA
//
//  Created by Dmitry Roytman on 12.05.2022.
//

import SwiftUI
import CoreLocation

struct LocationInfoView: View {
  let location: CLLocationCoordinate2D
  var body: some View {
    Text("Your coordinates are: \(location.longitude), \(location.latitude)")
  }
}
