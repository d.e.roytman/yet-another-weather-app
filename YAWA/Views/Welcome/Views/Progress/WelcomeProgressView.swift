//
//  WelcomeProgressView.swift
//  YAWA
//
//  Created by Dmitry Roytman on 12.05.2022.
//

import SwiftUI

struct WelcomeProgressView: View {
  @Binding var isPresented: Bool
  var body: some View {
    VStack {
      if isPresented {
        ProgressView("Loading...")
          .foregroundColor(.primary)
          .padding(.bottom)
      }
    }
  }
}

struct WelcomeProgressView_Previews: PreviewProvider {
  static var previews: some View {
    WelcomeProgressView(isPresented: .constant(true))
  }
}
