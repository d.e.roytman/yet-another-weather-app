//
//  CurrentLocationButton.swift
//  YAWA
//
//  Created by Dmitry Roytman on 12.05.2022.
//

import SwiftUI
import CoreLocationUI

struct CurrentLocationButton: View {
  @EnvironmentObject var locationStore: LocationStore
  var body: some View {
    LocationButton(.shareCurrentLocation) {
      locationStore.requestLocation()
    }
    .cornerRadius(.cornerRadius)
    .symbolVariant(.fill)
    .foregroundColor(.white)
  }
}

struct CurrentLocationButton_Previews: PreviewProvider {
  static var previews: some View {
    CurrentLocationButton()
      .environmentObject(LocationStore())
  }
}

extension CGFloat {
  fileprivate static var cornerRadius: CGFloat { 30 }
}
