//
//  WelcomeHeaderView.swift
//  YAWA
//
//  Created by Dmitry Roytman on 12.05.2022.
//

import SwiftUI

struct WelcomeHeaderView: View {
  var body: some View {
    VStack(spacing: .spacing) {
      Text(string: .title)
        .font(.title.bold())
      
      Text(string: .description)
        .font(.headline)
    }
    .multilineTextAlignment(.center)
    .padding()
  }
}

struct WelcomeHeaderView_Previews: PreviewProvider {
  static var previews: some View {
    WelcomeHeaderView()
  }
}

// MARK: - Constants

extension CGFloat {
  fileprivate static var spacing: CGFloat { 20 }
}

extension String {
  fileprivate static var title: String {
    "Welcome to the Weather App"
  }
  fileprivate static var description: String {
    "Please share your current location to get the weather in your area"
  }
}
