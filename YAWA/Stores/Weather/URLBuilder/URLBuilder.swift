//
//  URLBuilder.swift
//  YAWA
//
//  Created by Dmitry Roytman on 09.05.2022.
//

import Foundation
import CoreLocation

protocol URLBuilderProtocol {
  typealias Latitude = CLLocationDegrees
  typealias Longitude = CLLocationDegrees
  func buildURL(latitude: Latitude, longitude: Longitude) throws -> URL
}

enum URLBuilderError: Error {
  case buildingFromComponentsError
  case buildingFromBaseURLError
}

final class URLBuilder: URLBuilderProtocol {
  
  // MARK: - Private properties
  
  private let baseURL: URL
  private let apiKey: String
  
  // MARK: - Init
  
  init(baseURL: URL = .baseURL, apiKey: String = .apiKey) {
    self.baseURL = baseURL
    self.apiKey = apiKey
  }
  
  // MARK: - Internal methods
  
  func buildURL(latitude: Latitude, longitude: Longitude) throws -> URL {
    var components = try buildBaseURLComponents()
    components.queryItems = buildQueryItems(latitude: latitude, longitude: longitude)
    guard let url = components.url else {
      throw URLBuilderError.buildingFromComponentsError
    }
    return url
  }
  
  // MARK: - Private methods
  
  private func buildQueryItems(latitude: Latitude, longitude: Longitude) -> [URLQueryItem] {
    let latQuery = URLQueryItem(name: .lat, value: "\(latitude)")
    let lonQuery = URLQueryItem(name: .lon, value: "\(longitude)")
    let idQuery = URLQueryItem(name: .appid, value: apiKey)
    return [latQuery, lonQuery, idQuery, .unitsQuery]
  }
  
  private func buildBaseURLComponents() throws -> URLComponents {
    guard let components = URLComponents(url: .baseURL, resolvingAgainstBaseURL: true) else {
      throw URLBuilderError.buildingFromBaseURLError
    }
    return components
  }
}

// MARK: - Helpers

extension URLQueryItem {
  fileprivate static var unitsQuery: URLQueryItem {
    URLQueryItem(name: .units, value: .metric)
  }
}

extension URL {
  fileprivate static var baseURL: URL {
    guard let url = URL(string: .basePath) else {
      fatalError("Failed to create url from path <\(String.basePath)>")
    }
    return url
  }
}

extension String {
  fileprivate static var basePath: String {
    "https://api.openweathermap.org/data/2.5/weather"
  }
  fileprivate static var apiKey: String {
    fatalError("Implement")
  }
}

extension String {
  fileprivate static var lat: String { "lat" }
  fileprivate static var lon: String { "lon" }
  fileprivate static var appid: String { "appid" }
  fileprivate static var units: String { "units" }
  fileprivate static var metric: String { "metric" }
}
