//
//  WeatherStore.swift
//  YAWA
//
//  Created by Dmitry Roytman on 09.05.2022.
//

import Foundation
import CoreLocation

enum WeatherStoreError: Error {
  case general
  case network
  case dataFormat
  case unknown
}

final class WeatherStore: ObservableObject {
  
  // MARK: - Private properties
  
  private let urlBuilder: URLBuilderProtocol
  private let urlSession: URLSession
  private let decoder: JSONDecoder
  
  // MARK: - Internal properties
  
  // TODO: - replace these 2 variables with a single state
  @Published var weather: Weather?
  @Published var error: WeatherStoreError?
  
  // MARK: - Init
  
  init(
    urlBuilder: URLBuilderProtocol = URLBuilder(),
    urlSession: URLSession = .shared,
    decoder: JSONDecoder = .init()
  ) {
    self.urlBuilder = urlBuilder
    self.urlSession = urlSession
    self.decoder = decoder
  }
  
  // MARK: - Internal methods
  
  func requestCurrentWeather(
    latitude: CLLocationDegrees, longitude: CLLocationDegrees
  ) async {
    do {
      let url = try buildURL(latitude: latitude, longitude: longitude)
      let data = try await fetchData(with: url)
      weather = try decodeData(data)
    } catch error as WeatherStoreError {
      self.error = error
    } catch {
      self.error = .unknown
    }
  }
  
  // MARK: - Private methods
  
  private func buildURL(latitude: CLLocationDegrees, longitude: CLLocationDegrees) throws -> URL {
    do {
      let url = try urlBuilder.buildURL(latitude: latitude, longitude: longitude)
      return url
    } catch {
      self.error = .general
      throw WeatherStoreError.general
    }
  }
  
  private func fetchData(with url: URL) async throws -> Data {
    do {
      let (data, response) = try await urlSession.data(from: url)
      guard validateResponse(response) else {
        self.error = .network
        throw WeatherStoreError.network
      }
      return data
    } catch {
      self.error = .network
      throw WeatherStoreError.network
    }
  }
  
  private func decodeData(_ data: Data) throws -> Weather {
    do {
      let weather = try decoder.decode(Weather.self, from: data)
      return weather
    } catch {
      throw WeatherStoreError.dataFormat
    }
  }
  
  private func validateResponse(_ response: URLResponse) -> Bool {
    guard let httpResponse = response as? HTTPURLResponse else {
      assertionFailure("Unexpected casting error from response <\(response.debugDescription)> to HTTPURLResponse")
      // If there is no way to validate the response then it optimistically treats it as a valid
      return true
    }
    
    let isValid = 200..<300 ~= httpResponse.statusCode
    return isValid
  }
}
