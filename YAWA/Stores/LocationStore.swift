//
//  LocationStore.swift
//  YAWA
//
//  Created by Dmitry Roytman on 09.05.2022.
//

import Foundation
import Combine
import CoreLocation

final class LocationStore: NSObject, ObservableObject  {
  
  // MARK: - Private properties
  
  private let locationManager: CLLocationManager
  
  // MARK: - Internal properties
  
  @Published var location: CLLocationCoordinate2D?
  @Published var isLoading = false

  // MARK: - Init
  
  init(locationManager: CLLocationManager = .init()) {
    self.locationManager = locationManager
  }
  
  // MARK: - Internal methods
  
  func start() {
    locationManager.delegate = self
  }
  
  func stop() {
    locationManager.delegate = nil
  }
  
  func requestLocation() {
    isLoading = true
    locationManager.requestLocation()
  }

}

extension LocationStore: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    defer { isLoading = false }
    guard let location = locations.first else {
      // TODO: - Handle the error
      assertionFailure("Locations' array is not supposed to be empty")
      return
    }
    self.location = location.coordinate
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    isLoading = false
    // TODO: - Handle the error
    print("Location request was failed with error: <\(error.localizedDescription)>")
  }
}
